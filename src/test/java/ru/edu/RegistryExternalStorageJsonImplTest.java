package ru.edu;

import org.junit.Before;
import org.junit.Test;
import ru.edu.model.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class RegistryExternalStorageJsonImplTest {

    RegistryExternalStorage jsonParser = new RegistryExternalStorageJsonImpl();
    RegistryExternalStorage xmlParser = new RegistryExternalStorageXmlImpl();

    String outputFileJson = "output/artist_by_country.json";
    String inputFileXml = "input/cd_catalog.xml";

    @Test
    public void readFrom() {
        Registry expected = (Registry) xmlParser.readFrom(inputFileXml);
        jsonParser.writeTo(outputFileJson, expected);

        Registry actual = (Registry) jsonParser.readFrom(outputFileJson);
        assertEquals(expected.toString(), actual.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void readToNullTest() {
         jsonParser.readFrom(null);
    }

    @Test
    public void writeTo() {
        Registry expected = (Registry) xmlParser.readFrom(inputFileXml);
        jsonParser.writeTo(outputFileJson, expected);
        assertNotNull(outputFileJson);
    }

    @Test(expected = IllegalArgumentException.class)
    public void writeToNullTest() {
        jsonParser.writeTo(null,new Registry());
    }
}