package ru.edu;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Before;
import org.junit.Test;
import ru.edu.model.Catalog;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.Assert.*;

public class RegistryExternalGeneralMethodsTest {

    RegistryExternalGeneralMethods methods = new RegistryExternalGeneralMethods();

    String pathToInput = "input/inputStreamText.txt";
    String checkText = "Text for checking the method";

    @Test
    public void inputStreamToString() throws IOException {
        String actual = methods.inputStreamToString(new FileInputStream(pathToInput));
        assertEquals(checkText, actual);
    }
}