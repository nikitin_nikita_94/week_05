package ru.edu;

import org.junit.Before;
import org.junit.Test;
import ru.edu.model.Album;
import ru.edu.model.Artist;
import ru.edu.model.Country;
import ru.edu.model.Registry;

import java.io.File;

import static org.junit.Assert.*;

public class RegistryExternalStorageSerializableImplTest {

    public static final Album ALBUM = new Album();
    public static final Artist ARTIST = new Artist();
    public static final Country COUNTRY = new Country();
    public static final Registry REGISTRY = new Registry();

    RegistryExternalStorage serializable = new RegistryExternalStorageSerializableImpl();

    File file = new File("output/artist_by_country.serialized");

    @Before
    public void setUp() {
        ALBUM.setName("Illmatic");
        ALBUM.setYear(1991L);

        ARTIST.setName("Nas");
        ARTIST.addAlbums(ALBUM);

        COUNTRY.setName("USA");
        COUNTRY.addArtists(ARTIST);

        REGISTRY.addCountries(COUNTRY);
    }

    @Test
    public void readFrom() {
        Registry reg = (Registry) serializable.readFrom(String.valueOf(file));
        assertNotNull(reg);
        System.out.println(reg);
    }

    @Test(expected = IllegalArgumentException.class)
    public void readToNullTest() {
        serializable.readFrom(null);
    }

    @Test
    public void writeTo() {
        serializable.writeTo(String.valueOf(file), REGISTRY);
        assertNotNull(file);
    }

    @Test(expected = IllegalArgumentException.class)
    public void writeToNullTest() {
        serializable.writeTo(null,new Registry());
    }
}