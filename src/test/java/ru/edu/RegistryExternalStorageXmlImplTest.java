package ru.edu;

import org.junit.Test;
import ru.edu.model.Registry;

import java.io.File;

import static org.junit.Assert.*;

public class RegistryExternalStorageXmlImplTest {

    RegistryExternalStorage xml = new RegistryExternalStorageXmlImpl();
    Registry expected;

    File inputFileXml = new File("input/cd_catalog.xml");
    File outputFileXml = new File("output/artist_by_country.xml");

    @Test
    public void readFrom() {
        expected = (Registry) xml.readFrom(String.valueOf(inputFileXml));
        assertNotNull(expected);
    }
    @Test(expected = IllegalArgumentException.class)
    public void readToNullTest() {
        xml.readFrom(null);
    }

    @Test
    public void writeTo() {
        Registry artist = (Registry) xml.readFrom(String.valueOf(inputFileXml));
        xml.writeTo(String.valueOf(outputFileXml), artist);
        assertNotNull(outputFileXml);
    }

    @Test(expected = IllegalArgumentException.class)
    public void writeToNullTest() {
        xml.writeTo(null,new Registry());
    }
}