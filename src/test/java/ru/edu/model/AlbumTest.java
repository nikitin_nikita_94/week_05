package ru.edu.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
@RunWith(MockitoJUnitRunner.class)
public class AlbumTest {

    @Spy
    Album album = Mockito.spy(Album.class);


    @Test
    public void getName() {
       Mockito.when(album.getName()).thenReturn("Illmatic");
       assertEquals("Illmatic",album.getName());
    }

    @Test
    public void setName() {
        album.setName("Stillmatic");
        assertEquals("Stillmatic",album.getName());
        Mockito.verify(album,Mockito.times(1)).setName("Stillmatic");
    }

    @Test
    public void getYear() {
        Mockito.when(album.getYear()).thenReturn(2001L);
        assertEquals(2001L,album.getYear());
    }

    @Test
    public void setYear() {
        album.setYear(2001L);
        assertEquals(2001L,album.getYear());
        Mockito.verify(album,Mockito.times(1)).setYear(2001L);
    }

    @Test
    public void testEquals() {
        Album expected = album;
        assertEquals(expected, album);
    }

    @Test
    public void testHashCode() {
        album.setName("Illmatic");
        album.setYear(1991L);

        Album expected = album;

        assertEquals(expected.hashCode(),album.hashCode());
    }

    @Test
    public void testToString() {
        Mockito.when(album.toString()).thenReturn("Illmatic");
        String expected = album.toString();

        assertEquals(expected,album.toString());
    }
}