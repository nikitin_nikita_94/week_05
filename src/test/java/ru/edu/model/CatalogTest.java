package ru.edu.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class CatalogTest {

    @Spy
    Catalog catalog = Mockito.spy(Catalog.class);

    CD cd;

    @Before
    public void setUp() {
        cd = new CD();
        cd.setTitle("8 Mile");
        cd.setArtist("Eminem");
        cd.setCompany("ShadyRecords");
        cd.setCountry("USA");
        cd.setPrice(100.00);
        cd.setYear(1998);
    }

    @Test
    public void getCatalog() {
        Mockito.when(catalog.getCatalog()).thenReturn(Collections.singletonList(cd));
        assertEquals(cd,catalog.getCatalog().get(0));
    }

    @Test
    public void testToString() {
        catalog.getCatalog().add(cd);
        assertEquals(cd.toString(),catalog.getCatalog().get(0).toString());
    }
}