package ru.edu.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class CDTest {

    @Spy
    CD cd = Mockito.spy(CD.class);

    @Test
    public void getTitle() {
        Mockito.when(cd.getTitle()).thenReturn("Red");
        assertEquals(cd.getTitle(),"Red");
    }

    @Test
    public void setTitle() {
        cd.setTitle("Red");
        assertEquals(cd.getTitle(),"Red");
    }

    @Test
    public void getArtist() {
        Mockito.when(cd.getArtist()).thenReturn("The Communards");
        assertEquals(cd.getArtist(),"The Communards");
    }

    @Test
    public void setArtist() {
        cd.setArtist("The Communards");
        assertEquals(cd.getArtist(),"The Communards");
    }

    @Test
    public void getCountry() {
        Mockito.when(cd.getCountry()).thenReturn("UK");
        assertEquals(cd.getCountry(),"UK");
    }

    @Test
    public void setCountry() {
        cd.setCountry("UK");
        assertEquals(cd.getCountry(),"UK");
    }

    @Test
    public void getCompany() {
        Mockito.when(cd.getCompany()).thenReturn("London");
        assertEquals(cd.getCompany(),"London");
    }

    @Test
    public void setCompany() {
        cd.setCompany("London");
        assertEquals(cd.getCompany(),"London");
    }

    @Test
    public void getPrice() {
        Mockito.when(cd.getPrice()).thenReturn(2.0123);
        assertEquals(cd.getPrice(),2.0123,20123);
    }

    @Test
    public void setPrice() {
        cd.setPrice(2.0123);
        assertEquals(cd.getPrice(),2.0123,20123);
    }

    @Test
    public void getYear() {
        Mockito.when(cd.getYear()).thenReturn(1988);
        assertEquals(cd.getYear(),1988);
    }

    @Test
    public void setYear() {
        cd.setYear(1988);
        assertEquals(cd.getYear(),1988);
    }

    @Test
    public void testToString() {
        cd.setTitle("Red");
        cd.setArtist("The Communards");
        cd.setCountry("UK");
        cd.setCompany("London");
        cd.setPrice(2.0123);
        cd.setYear(1988);

        String expected = cd.toString();
        assertEquals(expected,cd.toString());
    }
}