package ru.edu.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class CountryTest {

    @Spy
    Country country = Mockito.spy(Country.class);
    Country expected;

    @Before
    public void setUp() throws Exception {
        country.setName("USA");
        country.addArtists(new Artist("O.C", new Album("Jewelz", 1997)));

        expected = country;
    }

    @Test
    public void getName() {
        assertEquals(country.getName(), "USA");
    }

    @Test
    public void setName() {
        country.setName("UA");
        assertEquals(country.getName(), "UA");
    }

    @Test
    public void getArtists() {
        Artist expected = country.getArtists().get(0);
        assertEquals(expected, country.getArtists().get(0));
    }

    @Test
    public void addArtists() {
        country.addArtists(new Artist("Eminem", new Album(" The Eminem show", 2002)));
        assertEquals(2, country.getArtists().size());
    }

    @Test
    public void testEquals() {
        assertEquals(expected, country);
    }

    @Test
    public void testHashCode() {
        assertEquals(expected.hashCode(),country.hashCode());
    }

    @Test
    public void testToString() {
        assertEquals(expected.toString(),country.toString());
    }
}