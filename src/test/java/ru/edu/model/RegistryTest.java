package ru.edu.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class RegistryTest {

    Registry registry = Mockito.spy(Registry.class);
    Country country;

    @Before
    public void setUp() throws Exception {
        registry.addCountries(new Country("USA",new Artist("O.C",new Album("Jewelz", 1997))));
        country = registry.getCountries().get(0);
    }

    @Test
    public void getCountries() {
        assertEquals(country,registry.getCountries().get(0));
    }

    @Test
    public void getCountCountry() {
        assertEquals(1,registry.getCountCountry());
    }

    @Test
    public void addCountries() {
        registry.addCountries(new Country("UE",new Artist("Eminem", new Album(" The Eminem show", 2002))));
        assertEquals(2,registry.getCountries().size());
        assertEquals(2,registry.getCountCountry());
    }

    @Test
    public void testToString() {
        Registry expected = registry;
        assertEquals(expected.toString(),registry.toString());
    }
}