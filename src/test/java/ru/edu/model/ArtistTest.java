package ru.edu.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Objects;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ArtistTest {

    @Spy
    Artist artist = Mockito.spy(Artist.class);

    @Spy
    Artist expected;

    String nameArtist = "Joe Cocker";

    Album artistAlbum = new Album("Unchain my heart", 1987);

    @Before
    public void setUp() {
        artist.setName(nameArtist);
        artist.addAlbums(artistAlbum);
    }

    @Test
    public void getName() {
        assertEquals(nameArtist, artist.getName());
    }

    @Test
    public void setName() {
        artist.setName(nameArtist + "Salivan");
        assertEquals(nameArtist + "Salivan", artist.getName());
    }

    @Test
    public void getAlbums() {
        assertEquals(artistAlbum, artist.getAlbums().get(0));
    }

    @Test
    public void addAlbums() {
        artist.addAlbums(artistAlbum);
        assertEquals(2, artist.getAlbums().size());
        assertEquals(artistAlbum, artist.getAlbums().get(1));
    }

    @Test
    public void testEquals() {
        expected = artist;

        assertEquals(expected, artist);
        assertEquals(expected.toString(),artist.toString());
    }

    @Test
    public void testHashCode() {
        expected = artist;
        assertEquals(expected.hashCode(),artist.hashCode());
    }

    @Test
    public void testToString() {
        String expected = artist.toString();
        assertEquals(expected, artist.toString());
    }
}