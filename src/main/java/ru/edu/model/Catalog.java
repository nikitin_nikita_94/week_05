package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.ArrayList;
import java.util.List;

@JacksonXmlRootElement(localName = "Catalog")
public class Catalog {

    @JacksonXmlElementWrapper(localName = "CD",useWrapping = false)
    @JacksonXmlProperty(localName = "CD")
    @JsonProperty("CD")
    private List<CD> cd = new ArrayList<>();

    public Catalog() {

    }

    public List<CD> getCatalog() {
        return cd;
    }

    @Override
    public String toString() {
        return "Catalog{" +
                "catalog=" + cd +
                '}';
    }
}
