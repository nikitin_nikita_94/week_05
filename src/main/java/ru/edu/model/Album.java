package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;
import java.util.Objects;

public class Album implements Serializable {

    private static final long serialVersionUID = 7138444300706345083L;

    @JsonProperty()
    @JacksonXmlProperty(localName = "name", isAttribute = true)
    private String name;

    @JsonProperty()
    @JacksonXmlProperty(localName = "year", isAttribute = true)
    private long year;

    public Album() {

    }

    public Album(String name, long year) {
        this.name = name;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getYear() {
        return year;
    }

    public void setYear(long year) {
        this.year = year;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Album album = (Album) o;
        return year == album.year && name.equals(album.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, year);
    }

    @Override
    public String toString() {
        return "Album{" +
                "name='" + name + '\'' +
                ", year=" + year +
                '}';
    }
}
