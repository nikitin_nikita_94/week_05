package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Country implements Serializable {

    private static final long serialVersionUID = -5362833092825597241L;

    @JsonProperty()
    @JacksonXmlProperty(localName = "name")
    private String name;

    @JsonProperty()
    @JacksonXmlElementWrapper(localName = "Artist",useWrapping = false)
    @JacksonXmlProperty(localName = "Artist")
    private List<Artist> artists = new ArrayList<>();

    public Country() {

    }

    public Country(String name) {
        this.name = name;
    }

    public Country(String name, Artist artist) {
        this.name = name;
        addArtists(artist);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Artist> getArtists() {
        return artists;
    }

    public void addArtists(Artist artist) {
        this.artists.add(artist);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Country country = (Country) o;
        return Objects.equals(name, country.name) && Objects.equals(artists, country.artists);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, artists);
    }

    @Override
    public String toString() {
        return "Country{" +
                "name='" + name + '\'' +
                ", artists=" + artists +
                '}';
    }
}
