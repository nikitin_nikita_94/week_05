package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JacksonXmlRootElement(localName = "ArtistRegistry")
public class Registry implements Serializable {

    private static final long serialVersionUID = 1525419041170945423L;

    @JacksonXmlProperty(isAttribute = true)
    private int countCountry;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JsonProperty("Country")
    private List<Country> countries = new ArrayList<>();

    public Registry() {

    }

    public List<Country> getCountries() {
        return countries;
    }

    public int getCountCountry() {
        return countCountry;
    }

    public void addCountries(Country count) {
        countries.add(count);
        countCountry++;
    }

    @Override
    public String toString() {
        return "Registry{" +
                "countries=" + countries +
                '}';
    }
}
