package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Artist implements Serializable {

    private static final long serialVersionUID = 3574778895719288031L;

    @JsonProperty()
    @JacksonXmlProperty(localName = "Name")
    private String name;

    @JsonProperty()
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "Album")
    private List<Album> albums = new ArrayList<>();

    public Artist() {

    }

    public Artist(String name) {
        this.name = name;
    }

    public Artist(String name,Album album) {
        this.name = name;
        this.albums.add(album);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Album> getAlbums() {
        return albums;
    }

    public void addAlbums(Album album) {
        this.albums.add(album);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Artist artist = (Artist) o;
        return Objects.equals(name, artist.name) && Objects.equals(albums, artist.albums);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, albums);
    }

    @Override
    public String toString() {
        return "Artist{" +
                "name='" + name + '\'' +
                ", albums=" + albums +
                '}';
    }
}
