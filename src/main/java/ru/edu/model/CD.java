package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CD {
    @JsonProperty("TITLE")
    private String title;

    @JsonProperty("ARTIST")
    private String artist;

    @JsonProperty("COUNTRY")
    private String country;

    @JsonProperty("COMPANY")
    private String company;

    @JsonProperty("PRICE")
    private double price;

    @JsonProperty("YEAR")
    private int year;

    public CD() {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "CD{" +
                "title='" + title + '\'' +
                ", artist='" + artist + '\'' +
                ", country='" + country + '\'' +
                ", company='" + company + '\'' +
                ", price=" + price +
                ", year=" + year +
                '}';
    }
}
