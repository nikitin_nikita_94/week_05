package ru.edu;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.edu.model.*;

import java.io.*;
import java.util.*;

public class RegistryExternalStorageXmlImpl implements RegistryExternalStorage {

    RegistryExternalGeneralMethods generalMethods = new RegistryExternalGeneralMethods();

    /**
     * Чтение из файла.
     *
     * @param filePath путь до файла
     */
    @Override
    public Registry readFrom(String filePath) {
        if (filePath == null) {
            throw new IllegalArgumentException("Path to File equals NULL");
        }

        XmlMapper xmlMapper = new XmlMapper();
        try {
            String xml = generalMethods
                    .inputStreamToString(new FileInputStream(filePath));
            Catalog catalog = xmlMapper.readValue(xml, Catalog.class);
            return generalMethods.groupArtistByCountry(catalog);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Запись реестра в файл.
     *
     * @param filePath путь
     * @param registry реестр
     */
    @Override
    public void writeTo(String filePath, Registry registry) {
        if (filePath == null) {
            throw new IllegalArgumentException("Path to File equals NULL");
        }

        XmlMapper xmlMapper = new XmlMapper();
        try {
            xmlMapper.writeValue(new File(filePath), registry);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
