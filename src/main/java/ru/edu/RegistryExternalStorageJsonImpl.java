package ru.edu;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.edu.model.*;

import java.io.*;

public class RegistryExternalStorageJsonImpl implements RegistryExternalStorage {

    RegistryExternalGeneralMethods generalMethods = new RegistryExternalGeneralMethods();

    /**
     * Чтение из файла.
     *
     * @param filePath путь до файла
     */
    @Override
    public Registry readFrom(String filePath) {

        if (filePath == null) {
            throw new IllegalArgumentException("Path to File equals NULL");
        }

        ObjectMapper mapper = new ObjectMapper();
        try {
            String json = generalMethods
                    .inputStreamToString(new FileInputStream(filePath));
            Registry registry = mapper.readValue(json, Registry.class);
            return registry;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Запись реестра в файл.
     *
     * @param filePath путь
     * @param registry реестр
     */
    @Override
    public void writeTo(String filePath, Registry registry) {
        if (filePath == null) {
            throw new IllegalArgumentException("Path to File equals NULL");
        }

        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(new File(filePath), registry);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
