package ru.edu;

import ru.edu.model.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

public class RegistryExternalGeneralMethods {

    protected String inputStreamToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        return sb.toString();
    }

    protected Registry groupArtistByCountry(Catalog catalog) {
        Map<Artist, Country> mapRegister = new HashMap<>();
        catalog.getCatalog().forEach(i -> {
            mapRegister.put(new Artist(i.getArtist(),
                            new Album(i.getTitle(), i.getYear())),
                    new Country(i.getCountry()));
        });

        Set<Country> countriesFilter = catalog.getCatalog().stream()
                .map(cd -> new Country(cd.getCountry()))
                .distinct()
                .collect(Collectors.toSet());

        mapRegister.forEach((key, value) -> {
            countriesFilter.forEach(i -> {
                if (i.getName().equals(value.getName())) {
                    i.addArtists(key);
                }
            });
        });

        Registry artistCountry = new Registry();
        countriesFilter.stream()
                .forEach(i -> {
                    artistCountry.addCountries(i);
                });
        return artistCountry;
    }
}
