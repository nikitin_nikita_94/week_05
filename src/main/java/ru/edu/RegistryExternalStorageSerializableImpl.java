package ru.edu;

import ru.edu.model.Country;
import ru.edu.model.Registry;

import java.io.*;

public class RegistryExternalStorageSerializableImpl implements
        RegistryExternalStorage, Serializable {

    private static final long serialVersionUID = -1719147679352581525L;

    /**
     * Чтение из файла.
     *
     * @param filePath путь до файла
     */
    @Override
    public Registry readFrom(String filePath) {
        if (filePath == null) {
            throw new IllegalArgumentException("Path to File equals NULL");
        }

        Registry registry;

        try (FileInputStream fis = new FileInputStream(filePath);
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            return registry = (Registry) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Запись реестра в файл.
     *
     * @param filePath путь
     * @param registry реестр
     */
    @Override
    public void writeTo(String filePath, Registry registry) {
        if (filePath == null) {
            throw new IllegalArgumentException("Path to File equals NULL");
        }

        try (FileOutputStream fos = new FileOutputStream(filePath);
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(registry);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
